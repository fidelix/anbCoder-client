from PyQt5 import QtCore, QtGui, QtWidgets
from QtSingleAppInstance import SingleApplication
import sys
import design
import os
import tempfile
import core
import logging
import logHelper

WIN_RUN_PATH = "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run"
APP_NAME = 'AnbCoder'


class AnbCoderGui(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(':/anbCoder-favicon.svg'))

        # Still not implemented for Mac and Linux
        # Mac: http://www.andreybutov.com/2015/10/18/how-do-i-make-my-qt-app-start-automatically-at-login/
        # Linux: ~/.config/autostart/ - Good practices: https://github.com/martinrotter/rssguard/issues/15
        if not sys.platform.startswith('win'):
            self.autostartCheckbox.setEnabled(False)

        # Build system tray
        trayMenu = QtWidgets.QMenu()
        trayMenu.addAction(self.actionShowGUI)
        trayMenu.addAction(self.actionPause)
        trayMenu.addAction(self.actionStop)
        trayMenu.addAction(self.actionQuit)
        self.sysTrayIcon = QtWidgets.QSystemTrayIcon(self)
        self.sysTrayIcon.setContextMenu(trayMenu)
        self.sysTrayIcon.setIcon(QtGui.QIcon.fromTheme(":/anbCoder-favicon.svg"))
        self.sysTrayIcon.activated.connect(self.showGUI)

        self.settings = self.loadAndRestoreSettings()

        logHelper.setup(self.tmpDirValue.text(), APP_NAME, self.logText)

        # Connections
        self.actionQuit.triggered.connect(QtWidgets.qApp.quit)
        self.actionShowGUI.triggered.connect(self.showGUI)
        self.sysTrayCheckbox.stateChanged.connect(lambda state: self.sysTrayIcon.setVisible(bool(state)))
        self.tmpDirBrowseButton.clicked.connect(self.tmpChooseDir)
        self.winRunSettings = QtCore.QSettings(WIN_RUN_PATH, QtCore.QSettings.NativeFormat)
        self.saveConfigFormButtons.accepted.connect(self.saveSettings)

        self.openAppDirButton.clicked.connect(
            lambda: QtGui.QDesktopServices.openUrl(QtCore.QUrl(
                "file:///{}".format(self.tmpDirValue.text()),
                QtCore.QUrl.TolerantMode)
            )
        )

    def showGUI(self):
        self.hide()
        self.showNormal()
        self.activateWindow()

    def closeEvent(self, event):
        reply = QtWidgets.QMessageBox.question(
            self,
            'Message',
            'Você tem certeza que deseja sair? Todas as tarefas serão canceladas',
            QtWidgets.QMessageBox.Yes,
            QtWidgets.QMessageBox.No
        )
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
            QtWidgets.qApp.quit()

        else:
            event.ignore()

    def changeEvent(self, event):
        if event.type() == QtCore.QEvent.WindowStateChange:
            if self.windowState() & QtCore.Qt.WindowMinimized:
                self.hide()
                self.sysTrayIcon.showMessage(APP_NAME, 'Rodando no background...')

    def tmpChooseDir(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Selecione um diretório temporário")
        if directory:
            self.tmpDirValue.clear()
            self.tmpDirValue.insert(directory)

    def saveSettings(self):
        settings = self.settings
        settings.beginGroup("GUI")
        settings.setValue('size', self.size())
        settings.setValue('pos', self.pos())
        settings.setValue('sysTray', self.sysTrayCheckbox.isChecked())
        settings.endGroup()
        settings.beginGroup('Core')
        settings.setValue('priority', self.priorityValue.currentIndex())
        settings.setValue('cores', self.coresValue.value())
        settings.setValue('shareCPU', self.shareCPUCheckbox.isChecked())
        settings.setValue('privateKey', self.privateKeyValue.text())
        settings.setValue('tmp', self.tmpDirValue.text())
        settings.endGroup()

        if sys.platform.startswith('win'):
            self.winRunSettings.remove(APP_NAME)
            if self.autostartCheckbox.isChecked():
                self.winRunSettings.setValue(APP_NAME, os.path.realpath(__file__))  # needs debugging
            self.winRunSettings.sync()

        elif sys.platform.startswith('darwin'):
            # TODO
            pass

        elif sys.platform.startswith('linux'):
            # TODO
            return

    def loadAndRestoreSettings(self):
        settings = QtCore.QSettings('config.ini', QtCore.QSettings.IniFormat)

        # Autostart checkbox
        if sys.platform.startswith('win'):
            self.autostartCheckbox.setChecked(self.winRunSettings.contains(APP_NAME))  # needs TESTING
        elif sys.platform.startswith('darwin'):
            pass
        elif sys.platform.startswith('linux'):
            pass

        settings.beginGroup("GUI")
        # Restore window dimensions and position
        self.resize(settings.value('size', QtCore.QSize(640, 600), type=QtCore.QSize))
        self.move(settings.value('pos', type=QtCore.QPoint))
        isTrayVisible = settings.value('sysTrayIcon', True)
        self.sysTrayIcon.setVisible(isTrayVisible)
        self.sysTrayCheckbox.setChecked(isTrayVisible)
        settings.endGroup()

        settings.beginGroup('Core')
        self.priorityValue.setCurrentIndex(settings.value('priority', 0, type=int))
        self.shareCPUCheckbox.setChecked(settings.value('shareCPU', True, type=bool))
        self.coresValue.setValue(settings.value('cores', os.cpu_count(), type=int))
        self.privateKeyValue.setText(settings.value('privateKey'))
        self.tmpDirValue.setText(settings.value('tmp', tempfile.gettempdir()))
        settings.endGroup()

        return settings


def main():
    app = SingleApplication(sys.argv, APP_NAME)
    if app.isRunning():
        print(APP_NAME + ' já está rodando. Saindo...')
        sys.exit(1)

    gui = AnbCoderGui()
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
