import logging
import logging.handlers as handlers
from typing import Tuple
from PyQt5 import QtGui, QtWidgets


class QPlainTextEditLoggerHandler(logging.Handler):
    def __init__(self, widget):
        super().__init__()

        self.widget = widget

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)
        self.widget.moveCursor(QtGui.QTextCursor.End)
        scrollBar = self.widget.verticalScrollBar()
        scrollBar.setValue(scrollBar.maximum())

    def write(self, m):
        pass


def setup(tmpDir: str, filename: str, widget: QtWidgets.QPlainTextEdit):
    """

    :param str tmpDir: Temporary directory
    :param str filename: Filename to be used
    :param QPlainTextEdit widget:
    :return:
    """
    logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
    hdlr = handlers.RotatingFileHandler(
        "{}/{}.log".format(tmpDir.strip().rstrip('/'), filename),
        maxBytes=10000000,  # 10 MB
        backupCount=2
    )
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    # UI Logging
    QTextLoggerHandler = QPlainTextEditLoggerHandler(widget)
    QTextLoggerHandler.setFormatter(formatter)
    logger.addHandler(QTextLoggerHandler)
    logger.setLevel(logging.DEBUG)